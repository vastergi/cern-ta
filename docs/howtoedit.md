
# How to contribute to this web

# Basic edit 

## Modify existing files

This can be done in five steps as described below:

- **Step 1**: initial

Navigate to the page you want to modify and click on the "pencil" marker on the top right of the browser. 

![initiate edit](../images/HowToEdit_1.jpg)

This will bring you to the **GitLab** repository where all the contents of this web site are located. 

- **Step 2**: do the edit

Select the "Open in Web IDE" from the top right 

![Open in Web IDE](../images/HowToEdit_2.jpg)

This opens the page in the edit mode. You can type your text here and also preview the page - there are two tabs on the top to select

![Edit/Preview](../images/HowToEdit_3.jpg)

There is no need to save. The page is saved automatically as long as you remain in the browser. 

- **Step 3** : save the page

When you are satisfied with your modifications you need to save and commit the page to the web. For that you first select the "commit" button at the bottom left of the browser

![Crate commit](../images/HowToEdit_4.jpg)

and then select the option "commit to master" and hit "commit" again.

![Commit](../images/HowToEdit_5.jpg)

## Create new pages

...in preparation ...
